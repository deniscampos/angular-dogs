import { ColorEnum } from "../enums/color.enum";

export interface DogInterface {
    name: string;
    breed: string;
    font: string;
    color: ColorEnum;
    datetime: string;
    image: string;
}
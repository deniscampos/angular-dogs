import { Component, EventEmitter, Input, OnInit, Output, ElementRef, ViewChild } from '@angular/core';
import { nextTick } from 'q';

@Component({
    selector: 'app-ad-select',
    templateUrl: './ad-select.component.html',
    styleUrls: ['./ad-select.component.scss']
})
export class AdSelectComponent implements OnInit {
    @Input()
    items: string[] = [];

    @Input()
    styled: boolean = false;

    @Input()
    selected: string = '';

    @Output()
    output = new EventEmitter<any>();

    @ViewChild('search') search: ElementRef;
    
    private showList = false;
    private searchText = '';

    constructor() { }

    ngOnInit() {
        
    }

    open() {
        this.showList = true;

        nextTick(() => {
            (this.search.nativeElement as HTMLInputElement).focus();
        });
    }

    close() {
        this.showList = false;
    }

    select(item: string) {
        this.close();
        this.output.emit(item);
    }

    setItemStyle(item: string) {
        if (!this.styled || !item)
            return;

        return { 'font-family': `'${item}'` };
    }

    get itemsFiltered() {
        return this.items.filter(item => item.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()));
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSelectComponent } from './ad-select.component';

describe('AdSelectComponent', () => {
  let component: AdSelectComponent;
  let fixture: ComponentFixture<AdSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

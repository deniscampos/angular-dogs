import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdColorSelectComponent } from './ad-color-select.component';

describe('AdColorSelectComponent', () => {
  let component: AdColorSelectComponent;
  let fixture: ComponentFixture<AdColorSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdColorSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdColorSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

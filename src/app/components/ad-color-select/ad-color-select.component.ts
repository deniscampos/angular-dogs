import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ColorEnum } from 'src/app/enums/color.enum';

@Component({
    selector: 'app-ad-color-select',
    templateUrl: './ad-color-select.component.html',
    styleUrls: ['./ad-color-select.component.scss']
})
export class AdColorSelectComponent implements OnInit {
    @Input()
    selected: string = '';

    @Output()
    output = new EventEmitter<any>();
    
    constructor() { }

    ngOnInit() {}

    isColorSelected(color: string) {
        return color === this.selected;
    }
    
    setColor(color: string) {
        if (color !== this.selected) {
            this.output.emit(color);
            return;
        }

        this.output.emit('');
    }

    get colors() {
        return Object.keys(ColorEnum).map(key => ColorEnum[key]);
    }
}

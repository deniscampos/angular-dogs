import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FontEnum } from './enums/font.enum';
import { ApiResponse } from './models/api-response';
import { DogService } from './services/dog.service';
import { DogInterface } from './types/dog.interface';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'Angular Dogs';
    private listDogs: DogInterface[] = [];
    private listBreeds: string[] = [];
    private listFonts: FontEnum[] = [];
    private dog: DogInterface;
    private LOCAL_STORAGE_DOGS = 'dogs';

    constructor(private datePipe: DatePipe, private dogService: DogService) {
        
    }

    ngOnInit() {
        this.getDogsFromLocalStorage();
        this.setNewDog();
        this.listFonts = Object.keys(FontEnum).map(key => FontEnum[key]);
        this.dogService.breeds()
            .subscribe((data: ApiResponse) => {
                if (data.status === 'success') {
                    this.setBreeds(data.message);
                }
            });
    }

    setBreeds(apiResponse: any) {
        Object.keys(apiResponse).map(breed => {
            const breedValue = apiResponse[breed] as string[];
            
            if (!breedValue.length) {
                this.listBreeds.push(breed);
            
            } else {
                breedValue.forEach(subBreed => {
                    this.listBreeds.push(`${breed} - ${subBreed}`);
                });
            }
        });
    }

    setNewDog() {
        this.dog = {
            name: '',
            breed: '',
            color: null,
            font: ''
        } as DogInterface;
    }

    setBreed(breed: string) {
        this.dog.breed = breed;

        this.setImage();
    }

    setImage() {
        this.dogService.image(this.dog.breed.replace(/ /g, '').replace(/-/g, '/'))
            .subscribe((data: ApiResponse) => {
                if (data.status === 'success')
                    this.dog.image = data.message as string;
            });
    }

    saveDog() {
        this.dog.datetime = this.datePipe.transform(new Date(), 'dd-MM-yyyy HH:mm');
        this.listDogs.push(Object.assign({}, this.dog));
        localStorage.setItem(this.LOCAL_STORAGE_DOGS, JSON.stringify(this.listDogs));
        this.setNewDog();
    }

    dogStyle(dog: DogInterface) {
        return {
            'color': dog.color,
            'font-family': dog.font
        }
    }

    dogTitle(dog: DogInterface) {
        return `Nome: ${dog.name}\nRaça: ${dog.breed}\nAdicionado em: ${dog.datetime}`;
    }

    getDogsFromLocalStorage() {
        const localStorageDogs = JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_DOGS));
        this.listDogs = localStorageDogs || [];
    }

    removeAllDogs() {
        localStorage.removeItem(this.LOCAL_STORAGE_DOGS);
        this.getDogsFromLocalStorage();
    }

    getImage(dog: DogInterface) {
        if (!dog.image)
            return;

        return { 'background-image': `url(${dog.image})` };
    }

    get dogs() {
        return this.listDogs;
    }

    get breeds() {
        return this.listBreeds;
    }

    get fonts() {
        return this.listFonts;
    }
}

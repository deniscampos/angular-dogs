export enum ColorEnum {
    GREEN = '#ffaa00',
    YELLOW = '#eb4164',
    RED = '#9378FF',
    BLUE = '#45BCFF',
    PURPLE = '#14aa5a'
}
export enum FontEnum {
    CALISTOGA = 'Calistoga',
    ROBOTO = 'Roboto',
    ODIBEE = 'Odibee Sans',
    ROBOTO_SLAB = 'Roboto Slab',
    TOMORROW = 'Tomorrow'
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DogService {
    private URL = 'https://dog.ceo/api/';

    constructor(private http: HttpClient) {}

    breeds() {
        return this.http.get(this.URL + 'breeds/list/all');
    }

    image(breed: string) {
        return this.http.get(this.URL + `breed/${breed}/images/random`);
    }
}
